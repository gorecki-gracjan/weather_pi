import weather as weather_module
import datetime

def build_weather_layout(weather):
    
    weather_text = f"{weather['name']} {weather['main']['temp']} C \n"
    weather_text += f"{weather['weather'][0]['description']} \n"
    weather_text += f"min: {weather['main']['temp_min']} max: {weather['main']['temp_max']} \n"
    weather_text += f"odczuwalna {weather['main']['feels_like']} \n"
    weather_text += f"\n {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')}"
    # print(weather['sys']['sunrise'])
    # print(weather['sys']['sunset'])
    return weather_text