#!/usr/bin/python3
# -*- coding:utf-8 -*-
import sys
import os
import time
import json
from PIL import Image,ImageDraw,ImageFont
from pathlib import Path
import weather as weather_module
import weather_polish
import time
from lib import epd4in2

def draw_text(xy, text, context, font, fill = 0):
    context.text(xy, text, fill, font)

def load_config(path):
    file = open(path, encoding='utf-8');
    return json.load(file)

font_dir = str(Path.home()) + '/weather_pi/font/'
lib_dir = str(Path.home()) + '/weather_pi/lib/'

path_to_config = str(Path.home()) + '/.weather_config'
config = load_config(path_to_config)

key = config['key']
city = config['city']
weather_url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={key}&units=metric&lang=pl';
font24 = ImageFont.truetype(os.path.join(font_dir, 'Font.ttc'), 24)
font18 = ImageFont.truetype(os.path.join(font_dir, 'Font.ttc'), 18)

width = 400
height = 300

epd = epd4in2.EPD()
epd.init()
width = epd.width
height = epd.height

while True:
    epd.Clear()
    Himage = Image.new('1', (width, height), 255)  # 255: clear the frame
    weather = weather_module.get_weather(weather_url)
    weather_text = weather_polish.build_weather_layout(weather)

    icon_id = weather['weather'][0]['icon']
    icon_url = f'http://openweathermap.org/img/wn/{icon_id}@2x.png'
    icon = weather_module.get_weather_icon(icon_url)

    context = ImageDraw.Draw(Himage)

    x = 40
    y = 80

    draw_text((x,y), weather_text,context, font24)
    context.bitmap((x+220, y+10),icon)
    epd.display(epd.getbuffer(Himage))
    # Himage.show()
    time.sleep(60 * 2)