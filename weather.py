import json
import requests
import urllib.request
import io
from PIL import Image

def get_weather(url):
    return requests.get(url).json()

def get_weather_icon(url):
    with urllib.request.urlopen(url) as url:
        f = io.BytesIO(url.read())
    return Image.open(f);